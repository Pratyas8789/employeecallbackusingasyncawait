const mainFunction = require('../main');
const path = require('path');
const { error } = require('console');

async function readingDirectory() {
    try {
        const response1 = await mainFunction.retrieve([2, 13, 23]);
        const response2 = await mainFunction.groupDataBasedOnCompanies(response1);
        const response3 = await mainFunction.powerpuffBrigadeCompanyData(response2);
        const response4 = await mainFunction.removeEntry(response3);
        const response5 = await mainFunction.sortData(response4);
        const response6 = await mainFunction.swapPosition(response5);
        const response7 = await mainFunction.addBirthday(response6);
        console.log(response7);
    }
    catch (error) {
        console.error(error);
    }
}

readingDirectory();